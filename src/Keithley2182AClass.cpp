static const char *ClassId    = "$Id: Keithley2182AClass.cpp,v 1.1 2013-02-14 14:53:51 olivierroux Exp $";
static const char *TagName    = "$Name: not supported by cvs2svn $";
static const char *CvsPath    = "$Source: /users/chaize/newsvn/cvsroot/Instrumentation/Keithley2182A/src/Keithley2182AClass.cpp,v $";
static const char *SvnPath    = "$HeadURL: $";
static const char *HttpServer = "http://www.esrf.fr/computing/cs/tango/tango_doc/ds_doc/";
//+=============================================================================
//
// file :        Keithley2182AClass.cpp
//
// description : C++ source for the Keithley2182AClass. A singleton
//               class derived from DeviceClass. It implements the
//               command list and all properties and methods required
//               by the Keithley2182A once per process.
//
// project :     TANGO Device Server
//
// $Author: olivierroux $
//
// $Revision: 1.1 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :   European Synchrotron Radiation Facility
//              BP 220, Grenoble 38043
//              FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>

#include <Keithley2182A.h>
#include <Keithley2182AClass.h>


//+----------------------------------------------------------------------------
/**
 *	Create Keithley2182AClass singleton and return it in a C function for Python usage
 */
//+----------------------------------------------------------------------------
extern "C" {
#ifdef WIN32

__declspec(dllexport)

#endif

	Tango::DeviceClass *_create_Keithley2182A_class(const char *name) {
		return Keithley2182A_ns::Keithley2182AClass::init(name);
	}
}


namespace Keithley2182A_ns
{
//+----------------------------------------------------------------------------
//
// method : 		AutoRangeONCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *AutoRangeONCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "AutoRangeONCmd::execute(): arrived" << endl;

	((static_cast<Keithley2182A *>(device))->auto_range_on());
	return new CORBA::Any();
}


//+----------------------------------------------------------------------------
//
// method : 		SelectCH1Class::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *SelectCH1Class::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "SelectCH1Class::execute(): arrived" << endl;

	((static_cast<Keithley2182A *>(device))->select_ch1());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		SelectCH2Cmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *SelectCH2Cmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "SelectCH2Cmd::execute(): arrived" << endl;

	((static_cast<Keithley2182A *>(device))->select_ch2());
	return new CORBA::Any();
}


//+----------------------------------------------------------------------------
//
// method : 		AutoRangeOFFCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *AutoRangeOFFCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "AutoRangeOFFCmd::execute(): arrived" << endl;

	((static_cast<Keithley2182A *>(device))->auto_range_off());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		RangeUPClass::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *RangeUPClass::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "RangeUPClass::execute(): arrived" << endl;

	((static_cast<Keithley2182A *>(device))->range_up());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		RangeDOWNCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *RangeDOWNCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "RangeDOWNCmd::execute(): arrived" << endl;

	((static_cast<Keithley2182A *>(device))->range_down());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		GetRangeClass::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *GetRangeClass::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "GetRangeClass::execute(): arrived" << endl;

	return insert((static_cast<Keithley2182A *>(device))->get_range());
}


//
//----------------------------------------------------------------
//	Initialize pointer for singleton pattern
//----------------------------------------------------------------
//
Keithley2182AClass *Keithley2182AClass::_instance = NULL;

//+----------------------------------------------------------------------------
//
// method : 		Keithley2182AClass::Keithley2182AClass(string &s)
// 
// description : 	constructor for the Keithley2182AClass
//
// in : - s : The class name
//
//-----------------------------------------------------------------------------
Keithley2182AClass::Keithley2182AClass(string &s):DeviceClass(s)
{

	cout2 << "Entering Keithley2182AClass constructor" << endl;
	set_default_property();
	get_class_property();
	write_class_property();
	
	cout2 << "Leaving Keithley2182AClass constructor" << endl;

}
//+----------------------------------------------------------------------------
//
// method : 		Keithley2182AClass::~Keithley2182AClass()
// 
// description : 	destructor for the Keithley2182AClass
//
//-----------------------------------------------------------------------------
Keithley2182AClass::~Keithley2182AClass()
{
	_instance = NULL;
}

//+----------------------------------------------------------------------------
//
// method : 		Keithley2182AClass::instance
// 
// description : 	Create the object if not already done. Otherwise, just
//			return a pointer to the object
//
// in : - name : The class name
//
//-----------------------------------------------------------------------------
Keithley2182AClass *Keithley2182AClass::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			string s(name);
			_instance = new Keithley2182AClass(s);
		}
		catch (bad_alloc)
		{
			throw;
		}		
	}		
	return _instance;
}

Keithley2182AClass *Keithley2182AClass::instance()
{
	if (_instance == NULL)
	{
		cerr << "Class is not initialised !!" << endl;
		exit(-1);
	}
	return _instance;
}

//+----------------------------------------------------------------------------
//
// method : 		Keithley2182AClass::command_factory
// 
// description : 	Create the command object(s) and store them in the 
//			command list
//
//-----------------------------------------------------------------------------
void Keithley2182AClass::command_factory()
{
	command_list.push_back(new SelectCH1Class("SelectCH1",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new SelectCH2Cmd("SelectCH2",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new AutoRangeONCmd("AutoRangeON",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new AutoRangeOFFCmd("AutoRangeOFF",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new RangeUPClass("RangeUP",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new RangeDOWNCmd("RangeDOWN",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new GetRangeClass("GetRange",
		Tango::DEV_VOID, Tango::DEV_STRING,
		"",
		"Range value",
		Tango::OPERATOR));

	//	add polling if any
	for (unsigned int i=0 ; i<command_list.size(); i++)
	{
	}
}

//+----------------------------------------------------------------------------
//
// method : 		Keithley2182AClass::get_class_property
// 
// description : 	Get the class property for specified name.
//
// in :		string	name : The property name
//
//+----------------------------------------------------------------------------
Tango::DbDatum Keithley2182AClass::get_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		Keithley2182AClass::get_default_device_property()
// 
// description : 	Return the default value for device property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum Keithley2182AClass::get_default_device_property(string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//+----------------------------------------------------------------------------
//
// method : 		Keithley2182AClass::get_default_class_property()
// 
// description : 	Return the default value for class property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum Keithley2182AClass::get_default_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		Keithley2182AClass::device_factory
// 
// description : 	Create the device object(s) and store them in the 
//			device list
//
// in :		Tango::DevVarStringArray *devlist_ptr : The device name list
//
//-----------------------------------------------------------------------------
void Keithley2182AClass::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{

	//	Create all devices.(Automatic code generation)
	//-------------------------------------------------------------
	for (unsigned long i=0 ; i < devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
						
		// Create devices and add it into the device list
		//----------------------------------------------------
		device_list.push_back(new Keithley2182A(this, (*devlist_ptr)[i]));							 

		// Export device to the outside world
		// Check before if database used.
		//---------------------------------------------
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(device_list.back());
		else
			export_device(device_list.back(), (*devlist_ptr)[i]);
	}
	//	End of Automatic code generation
	//-------------------------------------------------------------

}
//+----------------------------------------------------------------------------
//	Method: Keithley2182AClass::attribute_factory(vector<Tango::Attr *> &att_list)
//-----------------------------------------------------------------------------
void Keithley2182AClass::attribute_factory(vector<Tango::Attr *> &att_list)
{
	//	Attribute : channel
	channelAttrib	*channel = new channelAttrib();
	Tango::UserDefaultAttrProp	channel_prop;
	channel_prop.set_label("channel");
	channel_prop.set_format("1.0f");
	channel_prop.set_description("The current channel used for acquisition");
	channel->set_default_properties(channel_prop);
	att_list.push_back(channel);

	//	Attribute : value
	valueAttrib	*value = new valueAttrib();
	Tango::UserDefaultAttrProp	value_prop;
	value_prop.set_label("value");
	value_prop.set_unit("V");
	value_prop.set_standard_unit("V");
	value_prop.set_display_unit("V");
	value_prop.set_description("The value returned by the nanovoltmeter");
	value->set_default_properties(value_prop);
	att_list.push_back(value);

	//	Attribute : range
	rangeAttrib	*range = new rangeAttrib();
	Tango::UserDefaultAttrProp	range_prop;
	range_prop.set_label("range");
	range_prop.set_description("The current range used for the data acquired");
	range->set_default_properties(range_prop);
	att_list.push_back(range);

	//	Attribute : autoRange
	autoRangeAttrib	*auto_range = new autoRangeAttrib();
	Tango::UserDefaultAttrProp	auto_range_prop;
	auto_range_prop.set_label("autoRange");
	auto_range_prop.set_description("The autoRange status for the selected channel");
	auto_range->set_default_properties(auto_range_prop);
	att_list.push_back(auto_range);

	//	End of Automatic code generation
	//-------------------------------------------------------------
}

//+----------------------------------------------------------------------------
//
// method : 		Keithley2182AClass::get_class_property()
// 
// description : 	Read the class properties from database.
//
//-----------------------------------------------------------------------------
void Keithley2182AClass::get_class_property()
{
	//	Initialize your default values here (if not done with  POGO).
	//------------------------------------------------------------------

	//	Read class properties from database.(Automatic code generation)
	//------------------------------------------------------------------

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_class()->get_property(cl_prop);
	Tango::DbDatum	def_prop;
	int	i = -1;


	//	End of Automatic code generation
	//------------------------------------------------------------------

}

//+----------------------------------------------------------------------------
//
// method : 	Keithley2182AClass::set_default_property
// 
// description: Set default property (class and device) for wizard.
//              For each property, add to wizard property name and description
//              If default value has been set, add it to wizard property and
//              store it in a DbDatum.
//
//-----------------------------------------------------------------------------
void Keithley2182AClass::set_default_property()
{
	string	prop_name;
	string	prop_desc;
	string	prop_def;

	vector<string>	vect_data;
	//	Set Default Class Properties
	//	Set Default Device Properties
	prop_name = "GpibDeviceName";
	prop_desc = "This property contains the name of the GPIB device used to control the Nanovoltmeter";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

}
//+----------------------------------------------------------------------------
//
// method : 		Keithley2182AClass::write_class_property
// 
// description : 	Set class description as property in database
//
//-----------------------------------------------------------------------------
void Keithley2182AClass::write_class_property()
{
	//	First time, check if database used
	//--------------------------------------------
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	string	classname = get_name();
	string	header;
	string::size_type	start, end;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	string	str_title("Keithley2182A Class");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	vector<string>	str_desc;
	str_desc.push_back("This class aims to control a Keithley 2182A nanovoltmeter with a Gpib Device");
	description << str_desc;
	data.push_back(description);
		
	//	put cvs or svn location
	string	filename(classname);
	filename += "Class.cpp";
	
	// Create a string with the class ID to
	// get the string into the binary
	string	class_id(ClassId);
	
	// check for cvs information
	string	src_path(CvsPath);
	start = src_path.find("/");
	if (start!=string::npos)
	{
		end   = src_path.find(filename);
		if (end>start)
		{
			string	strloc = src_path.substr(start, end-start);
			//	Check if specific repository
			start = strloc.find("/cvsroot/");
			if (start!=string::npos && start>0)
			{
				string	repository = strloc.substr(0, start);
				if (repository.find("/segfs/")!=string::npos)
					strloc = "ESRF:" + strloc.substr(start, strloc.length()-start);
			}
			Tango::DbDatum	cvs_loc("cvs_location");
			cvs_loc << strloc;
			data.push_back(cvs_loc);
		}
	}
	// check for svn information
	else
	{
		string	src_path(SvnPath);
		start = src_path.find("://");
		if (start!=string::npos)
		{
			end = src_path.find(filename);
			if (end>start)
			{
				header = "$HeadURL: ";
				start = header.length();
				string	strloc = src_path.substr(start, (end-start));
				
				Tango::DbDatum	svn_loc("svn_location");
				svn_loc << strloc;
				data.push_back(svn_loc);
			}
		}
	}

	//	Get CVS or SVN revision tag
	
	// CVS tag
	string	tagname(TagName);
	header = "$Name: ";
	start = header.length();
	string	endstr(" $");
	
	end   = tagname.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strtag = tagname.substr(start, end-start);
		Tango::DbDatum	cvs_tag("cvs_tag");
		cvs_tag << strtag;
		data.push_back(cvs_tag);
	}
	
	// SVN tag
	string	svnpath(SvnPath);
	header = "$HeadURL: ";
	start = header.length();
	
	end   = svnpath.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strloc = svnpath.substr(start, end-start);
		
		string tagstr ("/tags/");
		start = strloc.find(tagstr);
		if ( start!=string::npos )
		{
			start = start + tagstr.length();
			end   = strloc.find(filename);
			string	strtag = strloc.substr(start, end-start-1);
			
			Tango::DbDatum	svn_tag("svn_tag");
			svn_tag << strtag;
			data.push_back(svn_tag);
		}
	}

	//	Get URL location
	string	httpServ(HttpServer);
	if (httpServ.length()>0)
	{
		Tango::DbDatum	db_doc_url("doc_url");
		db_doc_url << httpServ;
		data.push_back(db_doc_url);
	}

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	vector<string> inheritance;
	inheritance.push_back("Device_4Impl");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	//--------------------------------------------
	get_db_class()->put_property(data);
}

}	// namespace
