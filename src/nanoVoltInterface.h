//=============================================================================
// nanoVoltInterface.h
//=============================================================================
// abstraction.......This class aims to send commands to a nanovoltmeter
// class.............nanoVoltInterface 
// original author...SGARA NEXEYA
//=============================================================================
#ifndef _NANOVOLTINTERFACE_H
#define _NANOVOLTINTERFACE_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/InnerAppender.h>
#include <yat/utils/String.h>

namespace Keithley2182A_ns
{
	// ============================================================================
	// class: nanoVoltInterface
	// ============================================================================
	class nanoVoltInterface
	{
	public:
		//- ctor
		nanoVoltInterface(Tango::DeviceProxy * p_dev_proxy);
		//- dtor
		~nanoVoltInterface();
		//- read *IDN? for checking if the device is connected
		std::string read_id() throw (Tango::DevFailed);
		//- read_value
		double read_value() throw (Tango::DevFailed);
		//- read autorange for channel 1
		bool read_autorange_1() throw (Tango::DevFailed);
		//- read autorange for channel 2
		bool read_autorange_2() throw (Tango::DevFailed);
		//- set autorange for channel 1
		void set_autorange_1(bool pstate) throw (Tango::DevFailed);
		//- set autorange for channel 2
		void set_autorange_2(bool pstate) throw (Tango::DevFailed);
		//- read the current channel
		int read_channel() throw (Tango::DevFailed);
		//- read the current range for channel 1
		std::string read_range_1() throw (Tango::DevFailed);
		//- read the current range for channel 2
		std::string read_range_2() throw (Tango::DevFailed);
		//- set channel 1
		void set_channel_1() throw (Tango::DevFailed);
		//- set channel 2
		void set_channel_2() throw (Tango::DevFailed);
		//- set range for channel 1
		void set_range_1(std::string p_range) throw (Tango::DevFailed);
		//- set range for channel 2
		void set_range_2(std::string p_range) throw (Tango::DevFailed);
		//- get the range on a readable form from the set value
		std::string get_readable_range_from_set(std::string p_str_in);
	protected:
		//- get the range on a readable form
		std::string get_readable_range(std::string p_str_in);
	private:
		Tango::DeviceProxy * m_deviceProxy;
	};
}
#endif
