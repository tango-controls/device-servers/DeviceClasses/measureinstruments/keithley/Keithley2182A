//=============================================================================
//nanoVoltInterface.cpp
//=============================================================================
// abstraction.......This class aims to send commands to a nanovoltmeter
// class.............nanoVoltInterface
// original author...SGARA NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES 
// ============================================================================

#include <nanoVoltInterface.h>
#include <TypesAndConsts.h>

namespace Keithley2182A_ns
{

//-----------------------------------------------------------------------------
//- Check GPIB Device Proxy macro:
#define CHECK_PROXY \
    if (! this->m_deviceProxy) \
    { \
      THROW_DEVFAILED(_CPTC("INTERNAL_ERROR"), \
                      _CPTC("request aborted - the GPIB Device Proxy isn't properly initialized"), \
                      _CPTC("nanoVoltInterface::check_proxy")); \
    }

	//*****************************************************************************
	// nanoVoltInterface
	//*****************************************************************************
	// ============================================================================
	// nanoVoltInterface::nanoVoltInterface
	// ============================================================================
	nanoVoltInterface::nanoVoltInterface(Tango::DeviceProxy * p_dev_proxy)
	{
		m_deviceProxy = p_dev_proxy;
	}
	// ============================================================================
	// nanoVoltInterface::~nanoVoltInterface
	// ============================================================================
	nanoVoltInterface::~nanoVoltInterface()
	{

	}
	// ============================================================================
	// nanoVoltInterface::read_id
	// ============================================================================
	std::string nanoVoltInterface::read_id() 
		throw (Tango::DevFailed)
	{
    CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str;
		yat::String l_str_to_trim;
		l_str = "*IDN?";
		string l_string;

		l_data << l_str;

		try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the GPIB device"),
				_CPTC("nanoVoltInterface::read_id"));
		}

		l_data_ret >> l_string;
		l_str_to_trim = l_string.data();
		l_str_to_trim.trim();

		return l_str_to_trim;
	}	

	// ============================================================================
	// nanoVoltInterface::read_value
	// ============================================================================
	double nanoVoltInterface::read_value() 
		throw (Tango::DevFailed)
	{
    CHECK_PROXY;

		double l_ret = 0.0;
		yat::String l_str_to_trim;
		Tango::DeviceData l_data;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str(":SENS:DATA:FRESH?");
		string l_string;
		l_data << l_str;

		try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the GPIB device"),
				_CPTC("nanoVoltInterface::read_value"));
		}

		l_data_ret >> l_string;
		l_str_to_trim = l_string.data();
		l_str_to_trim.trim();

		if (strcmp(l_str_to_trim.data(),DATA_OVERFLOW) == 0)
		{
			THROW_DEVFAILED(_CPTC("HARDWARE_FAILURE"),
				_CPTC("The nanovoltmeter is in OVERFLOW"),
				_CPTC("nanoVoltInterface::read_Value"));
		}
		else
		{
			l_ret = atof(l_str_to_trim.data());
		}

		return l_ret;
	}

	// ============================================================================
	// nanoVoltInterface::read_autorange_1
	// ============================================================================
	bool nanoVoltInterface::read_autorange_1() 
		throw (Tango::DevFailed)
	{
    CHECK_PROXY;

		bool l_ret = false;
		Tango::DeviceData l_data;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str;
		string l_string;
		l_str = ":SENS:VOLT:CHAN1:RANG:AUTO?";
		
    l_data << l_str;

		try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the GPIB device"),
				_CPTC("nanoVoltInterface::read_autoRange"));
		}

		l_data_ret >> l_string;
		int l_res = atoi(l_string.data());

		if (l_res == 1)
		{
			l_ret = true;
		}

		return l_ret;
	}

	// ============================================================================
	// nanoVoltInterface::read_autorange_2
	// ============================================================================
	bool nanoVoltInterface::read_autorange_2() 
		throw (Tango::DevFailed)
	{
    CHECK_PROXY;

		bool l_ret = false;
		Tango::DeviceData l_data;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str;
		string l_string;
		l_str = ":SENS:VOLT:CHAN2:RANG:AUTO?";

		l_data << l_str;
		
    try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the GPIB device"),
				_CPTC("nanoVoltInterface::read_autoRange"));
		}
		
    l_data_ret >> l_string;
		int l_res = atoi(l_string.data());
		
    if (l_res == 1)
		{
			l_ret = true;
		}

		return l_ret;
	}

	// ============================================================================
	// nanoVoltInterface::set_autorange_1
	// ============================================================================
	void nanoVoltInterface::set_autorange_1(bool pstate) 
		throw (Tango::DevFailed)
	{
    CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DevString l_str;

		if (pstate)
		{
			l_str = ":SENS:VOLT:CHAN1:RANG:AUTO ON";
		} 
		else
		{
			l_str = ":SENS:VOLT:CHAN1:RANG:AUTO OFF";
		}

		l_data << l_str;
		
    try
		{
			m_deviceProxy->command_inout("Write",l_data );
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the GPIB device"),
				_CPTC("nanoVoltInterface::set_autorange_1"));
		}
	}

	// ============================================================================
	// nanoVoltInterface::set_autorange_2
	// ============================================================================
	void nanoVoltInterface::set_autorange_2(bool pstate) 
		throw (Tango::DevFailed)
	{
    CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DevString l_str;

		if (pstate)
		{
			l_str = ":SENS:VOLT:CHAN2:RANG:AUTO ON";
		} 
		else
		{
			l_str = ":SENS:VOLT:CHAN2:RANG:AUTO OFF";
		}

		l_data << l_str;

		try
		{
			m_deviceProxy->command_inout("Write",l_data );
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the GPIB device"),
				_CPTC("nanoVoltInterface::set_autorange_2"));
		}
	}

	// ============================================================================
	// nanoVoltInterface::read_channel
	// ============================================================================
	int nanoVoltInterface::read_channel() 
		throw (Tango::DevFailed)
	{
    CHECK_PROXY;

		int l_ret_val = 0;
		Tango::DeviceData l_data;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str(":SENS:CHAN?");
		string a_string;

		l_data << l_str;
		
    try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the GPIB device"),
				_CPTC("nanoVoltInterface::read_channel"));
		}
		
    l_data_ret >> a_string;
		int l_res = atoi(a_string.data());
		
    switch (l_res)
		{
		case 1 : l_ret_val = CHANNEL1;
			break;
		case 2 : l_ret_val = CHANNEL2;
			break;
		default: l_ret_val = CHANNEL_UNKNOWN;
		}

		return l_ret_val;
	}

	// ============================================================================
	// nanoVoltInterface::read_range_1
	// ============================================================================
	std::string nanoVoltInterface::read_range_1() 
		throw (Tango::DevFailed)
	{
    CHECK_PROXY;

		yat::String l_str_to_trim;
		Tango::DeviceData l_data;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str;
		l_str = ":SENS:VOLT:CHAN1:RANG?";
		string l_string;

		l_data << l_str;
		
    try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the GPIB device"),
				_CPTC("nanoVoltInterface::read_range_1"));
		}

		l_data_ret >> l_string;
		l_str_to_trim = l_string.data();
		l_str_to_trim.trim();
		l_str_to_trim = get_readable_range(l_str_to_trim);
		
    return l_str_to_trim;
	}

	// ============================================================================
	// nanoVoltInterface::read_range_2
	// ============================================================================
	std::string nanoVoltInterface::read_range_2() 
		throw (Tango::DevFailed)
	{
    CHECK_PROXY;

		Tango::DeviceData l_data;
		yat::String l_str_to_trim;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str;
		l_str = ":SENS:VOLT:CHAN2:RANG?";
		string l_string;

		l_data << l_str;
		
    try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the GPIB device"),
				_CPTC("nanoVoltInterface::read_range_2"));
		}

		l_data_ret >> l_string;
		l_str_to_trim = l_string.data();
		l_str_to_trim.trim();
		l_str_to_trim = get_readable_range(l_str_to_trim);
		
    return l_str_to_trim;
	}

	// ============================================================================
	// nanoVoltInterface::set_channel_1
	// ============================================================================
	void nanoVoltInterface::set_channel_1() 
		throw (Tango::DevFailed)
	{
    CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DevString l_str(":SENS:CHAN 1");

		l_data << l_str;
		
    try
		{
			m_deviceProxy->command_inout("Write",l_data );
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the GPIB device"),
				_CPTC("nanoVoltInterface::select_ch1"));
		}
	}

	// ============================================================================
	// nanoVoltInterface::set_channel_2
	// ============================================================================
	void nanoVoltInterface::set_channel_2() 
		throw (Tango::DevFailed)
	{
    CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DevString l_str(":SENS:CHAN 2");
		
    l_data << l_str;
		
    try
		{
			m_deviceProxy->command_inout("Write",l_data );
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the GPIB device"),
				_CPTC("nanoVoltInterface::select_ch2"));
		}
	}

	// ============================================================================
	// nanoVoltInterface::set_range_1
	// ============================================================================
	void nanoVoltInterface::set_range_1(std::string p_range) 
		throw (Tango::DevFailed)
	{
    CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DevString l_str;
		char l_char[50] = ":SENS:VOLT:CHAN1:RANG ";
		l_str = Tango::string_dup(strcat(l_char, p_range.data()));
		
    l_data << l_str;
		
    try
		{
			m_deviceProxy->command_inout("Write",l_data );
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the GPIB device"),
				_CPTC("nanoVoltInterface::set_range_1"));
		}
	}

	// ============================================================================
	// nanoVoltInterface::set_range_2
	// ============================================================================
	void nanoVoltInterface::set_range_2(std::string p_range) 
		throw (Tango::DevFailed)
	{
    CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DevString l_str;
		char l_char[50] = ":SENS:VOLT:CHAN2:RANG ";
		l_str = Tango::string_dup(strcat(l_char ,p_range.data()));
		
    l_data << l_str;
		
    try
		{
			m_deviceProxy->command_inout("Write",l_data );
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the GPIB device"),
				_CPTC("nanoVoltInterface::set_range_2"));
		}
	}

	// ============================================================================
	// nanoVoltInterface::get_readable_range
	// ============================================================================
	std::string nanoVoltInterface::get_readable_range(std::string p_str_in)
	{
		std::string	argout;
		if (strcmp(p_str_in.data(),R001V) == 0)
		{
			argout = RR001V;
		}
		if (strcmp(p_str_in.data(),R01V) == 0)
		{
			argout = RR01V;
		}
		if (strcmp(p_str_in.data(),R1V) == 0)
		{
			argout = RR1V;
		}
		if (strcmp(p_str_in.data(),R10V) == 0)
		{
			argout = RR10V;
		}
		if (strcmp(p_str_in.data(),R100V) == 0)
		{
			argout = RR100V;
		}
		return argout;
	}

	// ============================================================================
	// nanoVoltInterface::get_readable_range_from_set
	// ============================================================================
	std::string nanoVoltInterface::get_readable_range_from_set(std::string p_str_in)
	{
		std::string	argout;
		if (strcmp(p_str_in.data(),SETRANGE001V) == 0)
		{
			argout = RR001V;
		}
		if (strcmp(p_str_in.data(),SETRANGE01V) == 0)
		{
			argout = RR01V;
		}
		if (strcmp(p_str_in.data(),SETRANGE1V) == 0)
		{
			argout = RR1V;
		}
		if (strcmp(p_str_in.data(),SETRANGE10V) == 0)
		{
			argout = RR10V;
		}
		if (strcmp(p_str_in.data(),SETRANGE100V) == 0)
		{
			argout = RR100V;
		}
		return argout;
	}

}// namespace Keithley2182A_ns
