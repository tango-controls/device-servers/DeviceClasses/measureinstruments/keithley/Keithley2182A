//=============================================================================
// TypesAndConsts.h
//=============================================================================
// abstraction.......Definitions
// class.............TypesAndConsts 
// original author...SGARA NEXEYA
//=============================================================================
#ifndef _TYPES_AND_CONSTS_S
#define _TYPES_AND_CONSTS_S

namespace Keithley2182A_ns
{
// ============================================================================
// enum: eChannel
// ============================================================================
typedef enum eChannel
{
	CHANNEL_UNKNOWN = 0x0,
	CHANNEL1 = 0x1,
	CHANNEL2 = 0x2
};

//- readable range
#define RR100V "100V"
#define RR10V "10V"
#define RR1V "1V"
#define RR01V "100E-3V"
#define RR001V "10E-3V"

//- read range
#define R100V "100.000000"
#define R10V "10.000000"
#define R1V "1.000000"
#define R01V "0.100000"
#define R001V "0.010000"

#define DATA_OVERFLOW "+9.9E37"

//- set range
#define SETRANGE100V "100.0"
#define SETRANGE10V "10.0"
#define SETRANGE1V "1.0"
#define SETRANGE01V "0.1"
#define SETRANGE001V "0.01"

}

#endif // _TYPES_AND_CONSTS_S

