//=============================================================================
//
// file :         Keithley2182AClass.h
//
// description :  Include for the Keithley2182AClass root class.
//                This class is the singleton class for
//                the Keithley2182A device class.
//                It contains all properties and methods which the 
//                Keithley2182A requires only once e.g. the commands.
//			
// project :      TANGO Device Server
//
// $Author: olivierroux $
//
// $Revision: 1.1 $
// $Date: 2013-02-14 14:53:51 $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source: /users/chaize/newsvn/cvsroot/Instrumentation/Keithley2182A/src/Keithley2182AClass.h,v $
// $Log: not supported by cvs2svn $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#ifndef _KEITHLEY2182ACLASS_H
#define _KEITHLEY2182ACLASS_H

#include <tango.h>
#include <Keithley2182A.h>


namespace Keithley2182A_ns
{//=====================================
//	Define classes for attributes
//=====================================
class autoRangeAttrib: public Tango::Attr
{
public:
	autoRangeAttrib():Attr("autoRange", Tango::DEV_BOOLEAN, Tango::READ) {};
	~autoRangeAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Keithley2182A *>(dev))->read_autoRange(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Keithley2182A *>(dev))->is_autoRange_allowed(ty);}
};

class rangeAttrib: public Tango::Attr
{
public:
	rangeAttrib():Attr("range", Tango::DEV_STRING, Tango::READ) {};
	~rangeAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Keithley2182A *>(dev))->read_range(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Keithley2182A *>(dev))->is_range_allowed(ty);}
};

class valueAttrib: public Tango::Attr
{
public:
	valueAttrib():Attr("value", Tango::DEV_DOUBLE, Tango::READ) {};
	~valueAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Keithley2182A *>(dev))->read_value(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Keithley2182A *>(dev))->is_value_allowed(ty);}
};

class channelAttrib: public Tango::Attr
{
public:
	channelAttrib():Attr("channel", Tango::DEV_USHORT, Tango::READ) {};
	~channelAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Keithley2182A *>(dev))->read_channel(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Keithley2182A *>(dev))->is_channel_allowed(ty);}
};

//=========================================
//	Define classes for commands
//=========================================
class GetRangeClass : public Tango::Command
{
public:
	GetRangeClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	GetRangeClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~GetRangeClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<Keithley2182A *>(dev))->is_GetRange_allowed(any);}
};



class RangeDOWNCmd : public Tango::Command
{
public:
	RangeDOWNCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	RangeDOWNCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~RangeDOWNCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<Keithley2182A *>(dev))->is_RangeDOWN_allowed(any);}
};



class RangeUPClass : public Tango::Command
{
public:
	RangeUPClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	RangeUPClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~RangeUPClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<Keithley2182A *>(dev))->is_RangeUP_allowed(any);}
};



class AutoRangeOFFCmd : public Tango::Command
{
public:
	AutoRangeOFFCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	AutoRangeOFFCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~AutoRangeOFFCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<Keithley2182A *>(dev))->is_AutoRangeOFF_allowed(any);}
};



class AutoRangeONCmd : public Tango::Command
{
public:
	AutoRangeONCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	AutoRangeONCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~AutoRangeONCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<Keithley2182A *>(dev))->is_AutoRangeON_allowed(any);}
};



class SelectCH2Cmd : public Tango::Command
{
public:
	SelectCH2Cmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	SelectCH2Cmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~SelectCH2Cmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<Keithley2182A *>(dev))->is_SelectCH2_allowed(any);}
};



class SelectCH1Class : public Tango::Command
{
public:
	SelectCH1Class(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	SelectCH1Class(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~SelectCH1Class() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<Keithley2182A *>(dev))->is_SelectCH1_allowed(any);}
};



//
// The Keithley2182AClass singleton definition
//

class
#ifdef WIN32
	__declspec(dllexport)
#endif
	Keithley2182AClass : public Tango::DeviceClass
{
public:
//	properties member data

//	add your own data members here
//------------------------------------

public:
	Tango::DbData	cl_prop;
	Tango::DbData	cl_def_prop;
	Tango::DbData	dev_def_prop;

//	Method prototypes
	static Keithley2182AClass *init(const char *);
	static Keithley2182AClass *instance();
	~Keithley2182AClass();
	Tango::DbDatum	get_class_property(string &);
	Tango::DbDatum	get_default_device_property(string &);
	Tango::DbDatum	get_default_class_property(string &);
	
protected:
	Keithley2182AClass(string &);
	static Keithley2182AClass *_instance;
	void command_factory();
	void get_class_property();
	void attribute_factory(vector<Tango::Attr *> &);
	void write_class_property();
	void set_default_property();
	string get_cvstag();
	string get_cvsroot();

private:
	void device_factory(const Tango::DevVarStringArray *);
};


}	//	namespace Keithley2182A_ns

#endif // _KEITHLEY2182ACLASS_H
